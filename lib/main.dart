import 'package:capp4/Statics/uiElements.dart';
import 'package:capp4/UI/login.dart';
import 'package:capp4/UI/register.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

void main() {
  runApp(CAPP());
}

class CAPP extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      home: SplashScreen(
        seconds: 15,
        navigateAfterSeconds: EntryPoint(),
        loaderColor: CupertinoColors.activeBlue,
        title: Text("CAPP"),
      ),
    );
  }
}

class EntryPoint extends StatefulWidget {
  @override
  _EntryPointState createState() => _EntryPointState();
}

class _EntryPointState extends State<EntryPoint> {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                AppButton("Login", () => Navigator.of(context).push(
                  CupertinoPageRoute(builder: (context) => LoginPage()),
                )),
                AppButton("New User", () => Navigator.of(context).push(
                  CupertinoPageRoute(builder: (context) => RegisterPage()),
                )),
              ],
            ),
          ),
        ),
    );
  }
}
