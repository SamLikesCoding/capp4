import 'package:capp4/Models/Users.dart';
import 'package:capp4/Statics/uiElements.dart';
import 'package:capp4/UI/error.dart';
import 'package:capp4/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'message.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  String _name, _std_name, _email, _pswd, _cnf_pswd;
  bool isTCH, isAdmin, nextPage;
  var _usr;
  // ignore: non_constant_identifier_names
  TextEditingController name, std_name, email, pswd, cnf_pswd;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nextPage = true;
    isTCH = false;
    isAdmin = false;
    name = new TextEditingController();
    std_name = new TextEditingController();
    email = new TextEditingController();
    pswd = new TextEditingController();
    cnf_pswd = new TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Center(
          child: Container(
            width: 270,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                addUserSymbol(),
                nextPage? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    InputField("Name", name),
                    CupertinoSegmentedControl(
                        children: {
                          true: Text("  Teacher  "),
                          false: Text("   Parent   ")
                        },
                        groupValue: isTCH,
                        onValueChanged: (value) {
                          setState(() {
                            isTCH = value;
                          });
                        }),
                    !isTCH? InputField("Student's Name", std_name) : Padding(padding: EdgeInsets.all(2)),
                  ],
                ) : Column(
                  children: <Widget>[
                    InputField("Email", email),
                    PasswordField("Password", pswd),
                    PasswordField("Confirm Password", cnf_pswd),
                  ],
                ),
                AppButton(nextPage?"Submit":"Sign up", (){
                  // Register function
                  if(nextPage) {
                    _name = name.text;
                    if (_name == null) Navigator.of(context).push(
                        CupertinoPageRoute(builder: (context) => ErrorPage(
                            "Empty name field",
                            "The name field is empty, please complete to continue"
                        ))
                    );
                    if(!isTCH) {
                      _std_name = std_name.text;
                      if (_std_name == null) Navigator.of(context).push(
                          CupertinoPageRoute(builder: (context) => ErrorPage(
                              "Empty Student field",
                              "The Student name field is empty, please complete to continue"
                          ))
                      );
                    }
                    setState(() {
                      nextPage = false;
                    });
                  } else {
                    _email = email.text;
                    _pswd = pswd.text;
                    _cnf_pswd = cnf_pswd.text;

                    // Empty field
                    if(_email.isEmpty || _pswd.isEmpty || _cnf_pswd.isEmpty) Navigator.of(context).push(
                        CupertinoPageRoute(builder: (context) => ErrorPage(
                            "Empty fields",
                            "There are empty fields, please complete to continue"
                        ))
                    );

                    // Password mismatch
                    else if (_cnf_pswd != _pswd) Navigator.of(context).push(
                        CupertinoPageRoute(builder: (context) => ErrorPage(
                            "Mismatched Password",
                            "The passwords provided are mismatched"
                        ))
                    );

                    // Invalid Email
                    else if(!validEmail(_email))  Navigator.of(context).push(
                        CupertinoPageRoute(builder: (context) => ErrorPage(
                            "Invalid Email ID",
                            "The email id provided is invalid, please check email field"
                        ))
                    );

                    // Invalid Password
                    else if(!validPassword(_pswd) || _pswd.length < 8) Navigator.of(context).push(
                        CupertinoPageRoute(builder: (context) => ErrorPage(
                            "Invalid Password",
                            "The passwords should've \n"
                                "- Minimum 1 Special Character\n"
                                "- Minimum 1 Uppercase Letter\n"
                                "- Minimum 1 Lower Case Letter\n"
                                "- Minimum 1 Number\n"
                                "- Minimum length of 8 characters"
                        ))
                    );
                    // Green Flag
                    else {
                      if (isTCH) {
                        _usr = new TeacherUser(_name, _email);
                        print(_usr);
                      } else {
                        _usr = new ParentUser(_name, _std_name, _email);
                        print(_usr);
                      }
                      Navigator.of(context).pushReplacement(
                          CupertinoPageRoute(builder: (context) => MesgPage(
                              "We're almost there",""
                              "The signup was submitted to admin for validation,"
                              "Please wait for email confirmation ",
                              "Ok", () => Navigator.of(context).pushReplacement(
                            CupertinoPageRoute(
                                builder: (context) => EntryPoint(),
                              ),
                            ),
                          ))
                      );
                    }
                  }
                }),
              ],
            ),
          ),
      ),
    );
  }
}

// For password verification
bool validPassword(String password) {
  String pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
  RegExp regExp = new RegExp(pattern);
  return regExp.hasMatch(password);
}

bool validEmail(String email) {
  Pattern pattern =
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
      r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
      r"{0,253}[a-zA-Z0-9])?)*$";
  RegExp regExp = new RegExp(pattern);
  return regExp.hasMatch(email);
}