import 'package:capp4/Statics/uiElements.dart';
import 'package:flutter/cupertino.dart';

class MesgPage extends StatelessWidget {

  final String title, mesg, btnPlaceholder;
  final Function action;
  MesgPage(this.title, this.mesg, this.btnPlaceholder, this.action);

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Center(
        child: Container(
          width: 300,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              worked(),
              Header(title),
              Content(mesg),
              AppButton(btnPlaceholder, action),
            ],
          ),
        ),
      ),
    );
  }
}
