import 'package:capp4/Statics/uiElements.dart';
import 'package:capp4/UI/error.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  bool toPwd;
  TextEditingController usrID, pswd;
  String _usrID, _pswd;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    toPwd = false;
    usrID = new TextEditingController();
    pswd = new TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Center(
          child: Container(
            width: 270,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(padding: EdgeInsets.all(50)),
                !toPwd? InputField("Email-ID", usrID) : PasswordField("Password", pswd),
                Padding(padding: EdgeInsets.all(5)),
                AppButton(!toPwd? "Next":"Login", () {
                  if(!toPwd) {
                    _usrID = usrID.text;
                    if(_usrID.isNotEmpty) setState(() {
                      toPwd = true;
                    }); else Navigator.of(context).push(
                        CupertinoPageRoute(builder: (context) => ErrorPage(
                            "No UserID",
                            "You must enter User ID to continue"
                        )),
                    );
                  } else {
                    _pswd = pswd.text;
                    if(_pswd.isNotEmpty) {
                      // Auth action
                      print("Username :"+_usrID);
                      print("Password :"+_pswd);
                    } else Navigator.of(context).push(
                        CupertinoPageRoute(builder: (context) => ErrorPage(
                            "No Password",
                            "You must enter password to continue"
                        )));
                  }
                }),
                AppButton("Forgot Password", () {}),
              ],
            ),
          ),
        ),
    );
  }
}
