import 'package:flutter/cupertino.dart';
import 'package:capp4/Statics/uiElements.dart';


class ErrorPage extends StatelessWidget {

  final String title, message;
  ErrorPage(this.title, this.message);

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Center(
          child: Container(
            width: 300,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                errorSymbol(),
                Header(title),
                Content(message),
                AppButton("Ok", () => Navigator.of(context).pop())
              ],
            ),
          )
        ),
    );
  }
}
