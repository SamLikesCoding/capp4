class ParentUser {
  final String name, email, studentName;
  ParentUser(this.name, this.studentName, this.email);
}

class TeacherUser {
  final String name, email;
  TeacherUser(this.name, this.email);
}