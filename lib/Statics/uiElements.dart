import 'package:flutter/cupertino.dart';

// App Buttons
// ignore: must_be_immutable
class AppButton extends StatelessWidget {

  String placeholder;
  Function action;
  AppButton(this.placeholder, this.action);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(5.5),
        child: CupertinoButton(
          child: Text(placeholder),
          onPressed: action,
        ),
    );
  }
}

// ignore: must_be_immutable
class AppSymButton extends StatelessWidget {
  
  Icon symbol;
  Function action;
  AppSymButton(this.symbol, this.action);
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5.5),
      child: CupertinoButton(
        child: symbol,
        onPressed: action,
      ),
    );
  }
}

// Input forms
// ignore: must_be_immutable
class InputField extends StatelessWidget {

  String placeholder;
  TextEditingController textEditingController;
  InputField(this.placeholder, this.textEditingController);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(12.5),
      child: CupertinoTextField(
        placeholder: placeholder,
        controller: textEditingController,
      ),
    );
  }
}

// ignore: must_be_immutable
class PasswordField extends StatelessWidget {

  String placeholder;
  TextEditingController textEditingController;
  PasswordField(this.placeholder, this.textEditingController);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(12.5),
      child: CupertinoTextField(
        placeholder: placeholder,
        controller: textEditingController,
        obscureText: true,
      ),
    );
  }
}

// Icon Symbols
double symSize = 40.5;
var defaultColor = CupertinoColors.black;

// Error Symbol
Icon errorSymbol() => Icon(
    CupertinoIcons.clear_circled,
    size: symSize,
    color: defaultColor,
);

// Profile Symbol
Icon profileSymbol() => Icon(
    CupertinoIcons.profile_circled,
    size: symSize,
    color: defaultColor,
);

// Add User Symbol
Icon addUserSymbol() => Icon(
    CupertinoIcons.person_add,
    size: symSize,
    color: defaultColor,
);

Icon worked() => Icon(
  CupertinoIcons.check_mark_circled,
  size: symSize,
  color: defaultColor,
);

// Text Templates
// ignore: must_be_immutable
class Header extends StatelessWidget {

  String text;
  Header(this.text);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5.5),
      child: Text(
          text,
          style: TextStyle(
            fontSize: 20.6,
            fontWeight: FontWeight.bold,
          ),
      ),
    );
  }
}

// ignore: must_be_immutable
class Content extends StatelessWidget {

  String text;
  Content(this.text);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5.5),
      child: Text(
        text,
        style: TextStyle(
          fontSize: 18.6,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }
}

